﻿using Microsoft.AspNetCore.Mvc;
using NguyenMinhDuyThong;
using PhoneMobiles;
using PhoneMobiles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneMobiles.Controllers
{
    public class AuthController : Controller
    {
        private readonly MainDbContext _context;

        public AuthController(MainDbContext context)
        {
            _context = context;
            
        }

        public IActionResult Login()
        {
            return View();
        }

        public ActionResult Validate(Account account)
        {
            var admin = _context.Accounts.Where(s => s.Username == account.Username);
            if (admin.Any())
            {
                if (admin.Where(s => s.Password == account.Password).Any())
                {
                    return Json(new { status = true, message = "Login Successfull!" });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Password!" });
                }
            }
            else
            {
                return Json(new { status = false, message = "Invalid Username!" });
            }
        }
    }
}
