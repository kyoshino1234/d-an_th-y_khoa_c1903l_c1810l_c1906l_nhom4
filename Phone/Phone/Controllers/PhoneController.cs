﻿using Microsoft.AspNetCore.Mvc;
using NguyenMinhDuyThong.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenMinhDuyThong.Controllers
{
    public class PhoneController : Controller
    {
        private readonly PhoneRepository _phoneRepository = null;
        public PhoneController(PhoneRepository phoneRepository) 
        {
            _phoneRepository = phoneRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ViewResult> GetPhones(int id)
        {
            var data = await _phoneRepository.GetPhoneByID(id);

            return View(data);
        }
    }
}
