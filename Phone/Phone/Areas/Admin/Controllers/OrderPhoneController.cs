﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NguyenMinhDuyThong;
using PhoneMobiles.Models;

namespace PhoneMobiles.Areas.Admin.Controllers
{
    [Area("Admin")]
    //[Route("admin/")]
    public class OrderPhoneController : Controller
    {
        private readonly MainDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public OrderPhoneController(MainDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }


        // GET: OrderPhoneController
        public ActionResult Index()
        {
            List<Order> listOrder = null;
            listOrder = _context.Orders.ToList();
            if (listOrder == null)
            {
                return NotFound();
            }
            else
            {
                var model = listOrder;
                return View(model);
            }

        }

        // GET: OrderPhoneController/Details/5
        public ActionResult Details(int id)
        {


            return View();
        }

        // GET: OrderPhoneController/Create
        //[Route("CreateOrder")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderPhoneController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("CreateOrderPost")]
        public ActionResult Create(Order model)
        {
            try
            {
                Boolean flag = false;
                var isSuccess = false;
                if (ModelState.IsValid)
                {
                    return View(model);
                }
                else
                {
                    _context.Orders.Add(model);
                    _context.SaveChanges();
                    isSuccess = true;
                }
                if (isSuccess)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ModelState.AddModelError("", "Fail Create LeaveType");
                    return View(model);
                }



            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: {Failt ALl}", ex.ToString());
                return NotFound();
            }
        }

        // GET: OrderPhoneController/Edit/5
        //[Route("Edit")]
        
        public ActionResult Edit(String id)
        {
            Order item = null;
            if(id != null && id.Length > 0)
            {
                int idOrder = int.Parse(id);
                try
                {
                    item = _context.Orders.Find(idOrder);
                }catch(Exception ex)
                {
                    Console.WriteLine("Error: {Failt Order}", ex.ToString());
                }
            }
            if(item != null)
            {
                return View(item);
            }
            return NotFound();
        }

        // POST: OrderPhoneController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order model)
        {
            try
            {

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderPhoneController/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: OrderPhoneController/Delete/5
       /* [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/
    }
}
