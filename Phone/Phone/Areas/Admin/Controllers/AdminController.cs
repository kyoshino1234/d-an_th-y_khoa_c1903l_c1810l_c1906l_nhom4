﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NguyenMinhDuyThong;
using NguyenMinhDuyThong.Models;

namespace PhoneMobiles.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AdminController : Controller
    {
        private readonly MainDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public AdminController(MainDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }


        // GET: Admin/Admin
        public async Task<IActionResult> Index()
        {
            return View(await _context.Phones.ToListAsync());
        }

        
        [Route("Create")]

        // GET: Admin/Admin/Create
        public IActionResult Create()
        {
            return View();
        }
        [Route("CreatePhone")]
        // POST: Admin/Admin/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult>CreatePhone([Bind("IdPro,IdCate,NamePro,Price,Description,Screen,OperatingSystem,CameraFront,CameraRear,CPU,RAM,Memmory,Battery,ImageFile")] PhoneModels phoneModels)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
                if (phoneModels.ImageFile != null)
                {
                    //Save Image to wwwroot/image

                    string fileName = Path.GetFileNameWithoutExtension(phoneModels.ImageFile.FileName);
                    string extension = Path.GetExtension(phoneModels.ImageFile.FileName);
                    phoneModels.CoverImageUrl = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                    using (var fileStream = new FileStream(path, FileMode.Create))
                    {
                        await phoneModels.ImageFile.CopyToAsync(fileStream);

                    }

                    _context.Add(phoneModels);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
                return View(phoneModels);
        }
        [Route("Edit")]
        // GET: Admin/Admin/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phoneModels = await _context.Phones.FindAsync(id);
            if (phoneModels == null)
            {
                return NotFound();
            }
            return View(phoneModels);
        }
        [Route("EditPhone")]
        // POST: Admin/Admin/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPhone(int id, [Bind("IdPro,IdCate,NamePro,Price,Description,Screen,OperatingSystem,CameraFront,CameraRear,CPU,RAM,Memmory,Battery,ImageFile")] PhoneModels phoneModels)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var PhoneNameOld = await _context.Phones.Where(phone => phone.IdPro == phoneModels.IdPro).Select(phone => phone.CoverImageUrl).FirstOrDefaultAsync();
                    if (phoneModels.ImageFile != null)
                    {
                        //Save Image to wwwroot/image
                        string wwwRootPath = _hostEnvironment.WebRootPath;
                        string fileName = Path.GetFileNameWithoutExtension(phoneModels.ImageFile.FileName);
                        string extension = Path.GetExtension(phoneModels.ImageFile.FileName);
                        phoneModels.CoverImageUrl = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await phoneModels.ImageFile.CopyToAsync(fileStream);
                        }
                    }
                    else
                    {
                        phoneModels.CoverImageUrl = PhoneNameOld;
                    }
                    _context.Update(phoneModels);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PhoneModelsExists(phoneModels.IdPro))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(phoneModels);
        }


        // GET: Admin/Admin/Delete/5
        // POST: Admin/Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int? id)
        {
            var phoneModel = await _context.Phones.FindAsync(id);
            _context.Phones.Remove(phoneModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PhoneModelsExists(int id)
        {
            return _context.Phones.Any(e => e.IdPro == id);
        }
    }
}
