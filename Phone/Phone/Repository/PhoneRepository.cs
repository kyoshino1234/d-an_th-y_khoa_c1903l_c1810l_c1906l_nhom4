﻿using Microsoft.EntityFrameworkCore;
using NguyenMinhDuyThong.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenMinhDuyThong.Repository
{
    public class PhoneRepository

    {
        private readonly MainDbContext _context = null;
        public PhoneRepository(MainDbContext context) {
            _context = context;
        }
        //public async Task<List<PhoneModels>> GetAllPhones()
        //{
        //    var phones = new List<PhoneModels>();
        //    List<PhoneModels> allphones = await _context.Phones.ToListAsync();
        //    if (allphones?.Any() == true)
        //    {
        //        foreach (var phone in allphones)
        //        {
        //            phones.Add(new PhoneModels()
        //            {
        //                IdPro = phone.IdPro,
        //                NamePro = phone.NamePro,
        //                Price = phone.Price

        //            }); 
        //        }
        //    }
        //    return DataSource();
        //}
        public async Task<PhoneModels> GetPhoneByID(int id)
        {
            return await _context.Phones.Where(x => x.IdPro == id).Select(phone => new PhoneModels()
            {
                IdPro = phone.IdPro,
                NamePro = phone.NamePro,
                Description = phone.Description,
                Price = phone.Price,
                Screen = phone.Screen,
                OperatingSystem = phone.OperatingSystem,
                RAM = phone.RAM,
                CameraFront = phone.CameraFront,
                CameraRear = phone.CameraRear,
                CPU = phone.CPU,
                Memmory = phone.Memmory,
                Battery = phone.Battery,
                CoverImageUrl = phone.CoverImageUrl,
            }).FirstOrDefaultAsync();
        }
        //public List<PhoneModels> SearchPhone(String title)
        //{
        //    //return DataSource().Where(x => x.NamePro == title).ToList();
        //}
        
    }
}
