﻿using NguyenMinhDuyThong.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneMobiles.Models
{
    public class Order
    {
        [Key]
        public int IdOrder { get; set; }
        //public int IdOrderDetail { get; set; }
        //[ForeignKey("IdOrderDetail")]
        //public OrderDetail OrderDetail { get; set; }
        [DisplayName("Phone")]
        public int IdPro { get; set;}
        public PhoneModels Phone { get; set; }
        public int Total { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string TelePhone { get; set; }
        public string Email { get; set; }

        public int Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
