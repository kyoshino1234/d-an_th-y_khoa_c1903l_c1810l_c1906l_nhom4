﻿using Microsoft.AspNetCore.Http;
using PhoneMobiles.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenMinhDuyThong.Models
{
    public class PhoneModels
    {
        [Key]
        public int IdPro { get; set; }
        [Column(TypeName = "int")]
        public int IdCate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string NamePro { get; set; }
        [Column(TypeName = "int")]
        public int Price { get; set; }
        [Column(TypeName = "nvarchar(1000)")]
        public string Description { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string Screen { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string OperatingSystem { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CameraFront { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CameraRear { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CPU { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        
        public string RAM { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Memmory { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Battery { get; set; }
        [Column(TypeName = "nvarchar(1000)")]
        [ForeignKey("CategoryId")]
        public CategoryModels Category { get; set; }
        public string CoverImageUrl { get; set; }
        [NotMapped]
        [DisplayName("Upload File")]
        public IFormFile ImageFile { get; set; }
        
        
    }
}
