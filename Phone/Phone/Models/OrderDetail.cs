﻿using NguyenMinhDuyThong.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneMobiles.Models
{
    public class OrderDetail
    {
        [Key]
        public int IdOrderDetail { get; set; }
        public int Quantity { get; set; }
        public int IdPro { get; set; }

        [ForeignKey("IdPro")]
        public PhoneModels Phone { get; set; }
        public int Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
