﻿using Microsoft.EntityFrameworkCore;
using NguyenMinhDuyThong.Models;
using PhoneMobiles.Models;
using System.Collections.Generic;

namespace NguyenMinhDuyThong
{
    public class MainDbContext : DbContext
    {
        public MainDbContext(DbContextOptions<MainDbContext> options) : base(options) { }
        public DbSet<PhoneModels> Phones { get; set; }
        public DbSet<Account> Accounts { get; set; }

        public DbSet<Role> Roles { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(a =>
            {
                a.HasData(
                new Account
                {
                    Id = 1,
                    Username = "sysadmin",
                    Password = "sysadmin",
                    Roles = new List<Role>()
                }
                );
                ;
            }
            );
            modelBuilder.Entity<Role>(r =>
            {

                r.HasData(
                new Account
                {
                    Id = 1,
                    Username = "SYSADMIN"
                }
                );
            }
           );
        }

    }

}
