﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhoneMobiles.Migrations
{
    public partial class int0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrderDetails_IdOrderDetail",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_IdOrderDetail",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "Phone",
                table: "Orders",
                newName: "TelePhone");

            migrationBuilder.RenameColumn(
                name: "IdOrderDetail",
                table: "Orders",
                newName: "IdPro");

            migrationBuilder.AddColumn<int>(
                name: "PhoneIdPro",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PhoneIdPro",
                table: "Orders",
                column: "PhoneIdPro");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Phones_PhoneIdPro",
                table: "Orders",
                column: "PhoneIdPro",
                principalTable: "Phones",
                principalColumn: "IdPro",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Phones_PhoneIdPro",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_PhoneIdPro",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PhoneIdPro",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "TelePhone",
                table: "Orders",
                newName: "Phone");

            migrationBuilder.RenameColumn(
                name: "IdPro",
                table: "Orders",
                newName: "IdOrderDetail");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_IdOrderDetail",
                table: "Orders",
                column: "IdOrderDetail");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrderDetails_IdOrderDetail",
                table: "Orders",
                column: "IdOrderDetail",
                principalTable: "OrderDetails",
                principalColumn: "IdOrderDetail",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
