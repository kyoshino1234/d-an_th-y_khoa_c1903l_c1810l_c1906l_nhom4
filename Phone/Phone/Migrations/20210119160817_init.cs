﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhoneMobiles.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoryModels",
                columns: table => new
                {
                    IdCate = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryModels", x => x.IdCate);
                });

            migrationBuilder.CreateTable(
                name: "Phones",
                columns: table => new
                {
                    IdPro = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdCate = table.Column<int>(type: "int", nullable: false),
                    NamePro = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Price = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", nullable: true),
                    Screen = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    OperatingSystem = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    CameraFront = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CameraRear = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CPU = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RAM = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Memmory = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Battery = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    CategoryId = table.Column<int>(nullable: true),
                    CoverImageUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phones", x => x.IdPro);
                    table.ForeignKey(
                        name: "FK_Phones_CategoryModels_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "CategoryModels",
                        principalColumn: "IdCate",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Phones_CategoryId",
                table: "Phones",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Phones");

            migrationBuilder.DropTable(
                name: "CategoryModels");
        }
    }
}
